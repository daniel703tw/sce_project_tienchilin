import pandas as pd
import numpy as np
import unicodedata
from sklearn.linear_model import LinearRegression
from sklearn import preprocessing
import sklearn.gaussian_process as Gauss
import sklearn.neural_network as NN
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error as MSE
from gekko import GEKKO
class PIDtest:
    """
    PIDtest class consists of data-driven models generated from an open-access dataset and a simple PID control system.

    ...
    
    Attributes
    ----------
    link: str
        Access link to the dataset
    '''
    
    Methods
    ----------
        show_dataset(self):
            Show a raw open-access dataset
        get_theta_linearmodel(self,inputs,standardization,subtraction,printout):
            Get a linear model of the bending angle
        evaluate_theta_linearmodel(self,inputs,standardization,subtraction,printout):
            Print out the performance(mean squared error and standard deviation) of an linear model with test data
        get_flex_models(self,separate,printout):
            Get models of flex, but only exponential models will be returned for the PID system set-up
        auto_PID(self,separate,disp,case):
            Solve and simulate the PID control system with automatically determined PID parameters
        manual_PID(self,separate,disp,case,Kc,tauI,tauD):
            Solve and simulate the PID control system with manually determined PID parameters
        
    """
    
    def __init__(self):
        """Construct all the necessary attributes for the dataset

        """
        self.link = r"https://repository.lboro.ac.uk/ndownloader/files/9536965"
    
    def show_dataset(self):
        """Show a raw open-access dataset
        
        Returns
        ----------
            data.head(): pandas.DataFrame.head
                The first n rows of data
        """
        data = pd.read_excel(self.link)
        return data.head()
    
    def get_theta_linearmodel(self,inputs=3,standardization=False,subtraction=False,printout=True):
        """Get a linear model of the bending angle
        
        Parameters
        ----------
            inputs: int
                The number of input parameters
            standardization: bool
                Choose to standardize the dataset or not
            subtraction: bool
                Choose to subtract the tilting angel to get relative target angle or not
            printout: bool
                Choose to print out the results(Linear model formula, R2, Standard error) or not
        
        Returns
        ----------
            lin_reg: sklearn.linear_model._base.LinearRegression
                Ordinary least squares Linear Regression model
                
        Raises
        ----------
            TypeError: If inputs is not an integer, or if standardization, subtraction, or printout is not a boolean value
            ValueError: If inputs is not 1, 2, or 3
            Exception: If subtraction is activated when inputs=3(tilting angle is one of the input)

        """
        data = pd.read_excel(self.link,skiprows=2)
        data = data.drop(columns=data.columns[0])
        if not isinstance(inputs, int):
            raise TypeError('inputs must be an integer')
        if inputs > 3 or inputs < 1:
            raise ValueError('inputs should be 1, 2, or 3')
        if not isinstance(standardization, bool):
            raise TypeError('standardization must be True or False')
        if not isinstance(subtraction, bool):
            raise TypeError('standardization must be True or False')
        if not isinstance(printout, bool):
            raise TypeError('printout must be True or False')
        # Three possible inputs
        P_train = ['P (Psi)']
        F_train = ['Flex (v)']
        tilt_train = ['tilt (deg)']
        # Target output
        theta_train = ['theta (deg)']
        # Set up linear regression model
        lin_reg = LinearRegression()
        if inputs == 3:
            if subtraction == True:
                raise Exception('There is no need to subtract the tilting angle since it is one of the inputs now')
            x_train = pd.concat([data[P_train],data[F_train],data[tilt_train]],axis=1)
            y_train = data[theta_train]
            if standardization == True:
                x_train = x_train.apply(lambda x: (x-x.mean())/ x.std())
                y_train = y_train.apply(lambda x: (x-x.mean())/ x.std())
            lin_reg.fit(x_train, y_train)
            if printout == True:
                print('Coefficient for P, F,', unicodedata.lookup('greek small letter phi'),':' ,lin_reg.coef_.squeeze())
                print('Interception:', lin_reg.intercept_)
                print('R2:', lin_reg.score(x_train, y_train))
                print('Standard error:', np.std(np.array(y_train-lin_reg.predict(x_train))))   
        elif inputs == 2:
            x_train = pd.concat([data[P_train],data[F_train]],axis=1)
            y_train = data[theta_train]
            if subtraction == True:
                y_train = y_train.sub(np.array(data[tilt_train]),axis=0)
            if standardization == True:
                x_train = x_train.apply(lambda x: (x-x.mean())/ x.std())
                y_train = y_train.apply(lambda x: (x-x.mean())/ x.std())
            lin_reg.fit(x_train, y_train)
            if printout == True:
                print('Coefficient for P, F:' ,lin_reg.coef_.squeeze())
                print('Interception:', lin_reg.intercept_)
                print('R2:', lin_reg.score(x_train, y_train))
                print('Standard error:', np.std(np.array(y_train-lin_reg.predict(x_train))))   
        else:
            x_train = data[F_train]
            y_train = data[theta_train]
            if subtraction == True:
                y_train = y_train.sub(np.array(data[tilt_train]),axis=0)
            if standardization == True:
                x_train = x_train.apply(lambda x: (x-x.mean())/ x.std())
                y_train = y_train.apply(lambda x: (x-x.mean())/ x.std())
            lin_reg.fit(x_train, y_train)
            if printout == True:
                print('Coefficient for F:' ,lin_reg.coef_.squeeze())
                print('Interception:', lin_reg.intercept_)
                print('R2:', lin_reg.score(x_train, y_train))
                print('Standard error:', np.std(np.array(y_train-lin_reg.predict(x_train))))   
        return lin_reg
    
    def evaluate_theta_linearmodel(self,inputs=3,standardization=False,subtraction=False,printout=True):
        """Print out the performance(mean squared error and standard deviation) of an linear model with test data

        Parameters
        ----------
            inputs: int
                The number of input parameters
            standardization: bool
                Choose to standardize the dataset or not
            subtraction: bool
                Choose to subtract the tilting angel to get relative target angle or not
            printout: bool
                Choose to print out the previous results(Linear model formula, R2, Standard error) or not
                
        Raises
        ----------
            TypeError: If inputs is not an integer, or if standardization, subtraction, or printout is not a boolean value
            ValueError: If inputs is not 1, 2, or 3
        """
        data = pd.read_excel(self.link,skiprows=2)
        data = data.drop(columns=data.columns[0])
        if not isinstance(inputs, int):
            raise TypeError('inputs must be an integer')
        if inputs > 3 or inputs < 1:
            raise ValueError('inputs should be 1, 2, or 3')
        if not isinstance(standardization, bool):
            raise TypeError('standardization must be True or False')
        if not isinstance(subtraction, bool):
            raise TypeError('standardization must be True or False')
        if not isinstance(printout, bool):
            raise TypeError('printout must be True or False')
        P_test = ['P (Psi).1']
        F_test = ['Flex (v).1']
        tilt_test = ['tilt (deg).1']
        theta_test = ['theta (deg).1']
        lin_reg = self.get_theta_linearmodel(inputs,standardization,subtraction,printout)
        if inputs == 3:
            x_test = pd.concat([data[P_test],data[F_test],data[tilt_test]],axis=1).dropna()
            y_test = data[theta_test].dropna()
            if standardization == True:
                x_test = x_test.apply(lambda x: (x-x.mean())/ x.std())
                y_test = y_test.apply(lambda x: (x-x.mean())/ x.std())
            y_pred = lin_reg.predict(x_test)
            print('Mean squared error:', MSE(y_test, y_pred))
            print('Standard deviation:', np.std(np.array(y_test-y_pred)))
        elif inputs == 2:
            x_test = pd.concat([data[P_test],data[F_test]],axis=1).dropna()
            y_test = data[theta_test].dropna()
            if subtraction == True:
                y_test = y_test.sub(np.array(data[tilt_test].dropna()),axis=0)
            if standardization == True:
                x_test = x_test.apply(lambda x: (x-x.mean())/ x.std())
                y_test = y_test.apply(lambda x: (x-x.mean())/ x.std())
            y_pred = lin_reg.predict(x_test)
            print('Mean squared error:', MSE(y_test, y_pred))
            print('Standard deviation:', np.std(np.array(y_test-y_pred)))
        else:
            x_test = data[F_test].dropna()
            y_test = data[theta_test].dropna()
            if subtraction == True:
                y_test = y_test.sub(np.array(data[tilt_test].dropna()),axis=0)
            if standardization == True:
                x_test = x_test.apply(lambda x: (x-x.mean())/ x.std())
                y_test = y_test.apply(lambda x: (x-x.mean())/ x.std())
            y_pred = lin_reg.predict(x_test)
            print('Mean squared error:', MSE(y_test, y_pred))
            print('Standard deviation:', np.std(np.array(y_test-y_pred)))
            
    def get_flex_models(self,separate=False,printout=True):
        """Get models of flex, but only exponential models will be returned for the PID system set-up
        
        Parameters
        ----------
            separate: bool
                Choose to separate the dataset into two parts and derive two exponential regression models or not, this will also deactivate other regression models 
            printout: bool
                Choose to print out the results(Exponential model formula, R2, Standard error, scatter plot) or not
        
        Returns
        ----------
            z: numpy.ndarray
                Coefficients of the exponential regression model
            z_up, z_down: numpy.ndarray
                Coefficients of separate exponential regression models
        Raises
        ----------
            TypeError: If separate or printout is not a boolean value
            
        """
        if not isinstance(separate, bool):
            raise TypeError('separate must be True or False')
        if not isinstance(printout, bool):
            raise TypeError('printout must be True or False')
        data = pd.read_excel(self.link,skiprows=2)
        data = data.drop(columns=data.columns[0])
        P_train = ['P (Psi)']
        x_train = data[P_train]
        F_train = ['Flex (v)']
        y_train = data[F_train]
        # Gaussian process regression
        gpr = Gauss.GaussianProcessRegressor()
        gpr.fit(x_train,y_train)
        # Multi-layer perceptron regressor, also called artificial neural network(ANN)
        nn_reg = NN.MLPRegressor(hidden_layer_sizes=(7,100),random_state=1, max_iter=500).fit(x_train, np.ravel(y_train))
        # Exponential regression
        if separate == False:
            mean = np.sum(np.array(y_train))/len(y_train)
            z = np.polyfit(np.array(x_train).squeeze(),np.log(495-np.array(y_train).squeeze()),1)
            p = np.poly1d(z)
            logy_p = p(x_train)
            if printout == True:
                print('Exponential model: 495 - e^('+str(z[0])+' *P + '+str(z[1])+')')
                print('R2(Exponential):', 1-np.sum((np.array(y_train).transpose()-(495-np.exp(logy_p.squeeze())))**2)/np.sum((np.array(y_train)-mean)**2))
                print('Standard error(Exponential):', np.std(np.array(y_train).squeeze()-(495-np.exp(logy_p.squeeze()))))
                print('-------------------------------------------------')
                print('R2(Gauss):', gpr.score(x_train, y_train))
                print('Standard error(Gauss):', np.std(np.array(y_train-gpr.predict(x_train))))
                print('-------------------------------------------------')
                print('R2(ANN):', nn_reg.score(x_train, y_train))
                print('Standard error(ANN):', np.std(np.array(y_train)-nn_reg.predict(x_train)))
                #plt.figure(figsize=(10, 8))
                plt.scatter(x_train, y_train, c='b',alpha=0.5,label='Measurement')
                plt.scatter(np.array(x_train), gpr.predict(x_train),marker='o', c='r',linewidths=0.5,label='Gauss')
                plt.scatter(np.array(x_train), 495-np.exp(logy_p.squeeze()),marker='x',c='g',linewidths=0.5,label='Exponential')
                plt.scatter(np.array(x_train), nn_reg.predict(x_train),marker='*',c='orange',linewidths=0.5,label='ANN')
                plt.legend()
                plt.xlabel('Pressure (Psi)')
                plt.ylabel('Flex (v)')
                plt.show()
            return z
        else:
            # Divide into 7 groups according to the pressure range
            y_train_1 = y_train.iloc[x_train[x_train['P (Psi)']<=1].index]
            y_train_2 = y_train.iloc[x_train[(x_train['P (Psi)']>1) & (x_train['P (Psi)']<=2)].index]
            y_train_3 = y_train.iloc[x_train[(x_train['P (Psi)']>2) & (x_train['P (Psi)']<=3)].index]
            y_train_4 = y_train.iloc[x_train[(x_train['P (Psi)']>3) & (x_train['P (Psi)']<=4)].index]
            y_train_5 = y_train.iloc[x_train[(x_train['P (Psi)']>4) & (x_train['P (Psi)']<=5)].index]
            y_train_6 = y_train.iloc[x_train[(x_train['P (Psi)']>5) & (x_train['P (Psi)']<=6)].index]
            y_train_7 = y_train.iloc[x_train[x_train['P (Psi)']>6].index]
            x_train_1 = x_train[x_train['P (Psi)']<=1]
            x_train_2 = x_train[(x_train['P (Psi)']>1) & (x_train['P (Psi)']<=2)]
            x_train_3 = x_train[(x_train['P (Psi)']>2) & (x_train['P (Psi)']<=3)]
            x_train_4 = x_train[(x_train['P (Psi)']>3) & (x_train['P (Psi)']<=4)]
            x_train_5 = x_train[(x_train['P (Psi)']>4) & (x_train['P (Psi)']<=5)]
            x_train_6 = x_train[(x_train['P (Psi)']>5) & (x_train['P (Psi)']<=6)]
            x_train_7 = x_train[x_train['P (Psi)']>6]
            # Divide into sub-upper and sub-lower groups according to the mean
            mean1 = np.sum(np.array(y_train_1))/len(y_train_1)
            mean2 = np.sum(np.array(y_train_2))/len(y_train_2)
            mean3 = np.sum(np.array(y_train_3))/len(y_train_3)
            mean4 = np.sum(np.array(y_train_4))/len(y_train_4)
            mean5 = np.sum(np.array(y_train_5))/len(y_train_5)
            mean6 = np.sum(np.array(y_train_6))/len(y_train_6)
            mean7 = np.sum(np.array(y_train_7))/len(y_train_7)
            y_train_1_up = y_train_1[y_train_1['Flex (v)']>mean1]
            y_train_1_down = y_train_1[y_train_1['Flex (v)']<=mean1]
            y_train_2_up = y_train_2[y_train_2['Flex (v)']>mean2]
            y_train_2_down = y_train_2[y_train_2['Flex (v)']<=mean2]
            y_train_3_up = y_train_3[y_train_3['Flex (v)']>mean3]
            y_train_3_down = y_train_3[y_train_3['Flex (v)']<=mean3]
            y_train_4_up = y_train_4[y_train_4['Flex (v)']>mean4]
            y_train_4_down = y_train_4[y_train_4['Flex (v)']<=mean4]
            y_train_5_up = y_train_5[y_train_5['Flex (v)']>mean5]
            y_train_5_down = y_train_5[y_train_5['Flex (v)']<=mean5]
            y_train_6_up = y_train_6[y_train_6['Flex (v)']>mean6]
            y_train_6_down = y_train_6[y_train_6['Flex (v)']<=mean6]
            y_train_7_up = y_train_7[y_train_7['Flex (v)']>mean7]
            y_train_7_down = y_train_7[y_train_7['Flex (v)']<=mean7]
            x_train_1_up = x_train_1.loc[y_train_1[y_train_1['Flex (v)']>mean1].index]
            x_train_1_down = x_train_1.loc[y_train_1[y_train_1['Flex (v)']<=mean1].index]
            x_train_2_up = x_train_2.loc[y_train_2[y_train_2['Flex (v)']>mean2].index]
            x_train_2_down = x_train_2.loc[y_train_2[y_train_2['Flex (v)']<=mean2].index]
            x_train_3_up = x_train_3.loc[y_train_3[y_train_3['Flex (v)']>mean3].index]
            x_train_3_down = x_train_3.loc[y_train_3[y_train_3['Flex (v)']<=mean3].index]
            x_train_4_up = x_train_4.loc[y_train_4[y_train_4['Flex (v)']>mean4].index]
            x_train_4_down = x_train_4.loc[y_train_4[y_train_4['Flex (v)']<=mean4].index]
            x_train_5_up = x_train_5.loc[y_train_5[y_train_5['Flex (v)']>mean5].index]
            x_train_5_down = x_train_5.loc[y_train_5[y_train_5['Flex (v)']<=mean5].index]
            x_train_6_up = x_train_6.loc[y_train_6[y_train_6['Flex (v)']>mean6].index]
            x_train_6_down = x_train_6.loc[y_train_6[y_train_6['Flex (v)']<=mean6].index]
            x_train_7_up = x_train_7.loc[y_train_7[y_train_7['Flex (v)']>mean7].index]
            x_train_7_down = x_train_7.loc[y_train_7[y_train_7['Flex (v)']<=mean7].index]
            # Combine into upper and lower groups for generating regression models
            y_train_up = pd.concat([y_train_1_up,y_train_2_up,y_train_3_up,y_train_4_up,y_train_5_up,y_train_6_up,y_train_7_up])
            x_train_up = pd.concat([x_train_1_up,x_train_2_up,x_train_3_up,x_train_4_up,x_train_5_up,x_train_6_up,x_train_7_up])
            y_train_down = pd.concat([y_train_1_down,y_train_2_down,y_train_3_down,y_train_4_down,y_train_5_down,y_train_6_down,y_train_7_down])
            x_train_down = pd.concat([x_train_1_down,x_train_2_down,x_train_3_down,x_train_4_down,x_train_5_down,x_train_6_down,x_train_7_down])
            z_up = np.polyfit(np.array(x_train_up).squeeze(),np.log(495-np.array(y_train_up).squeeze()),1)
            p_up = np.poly1d(z_up)
            logy_p_up = p_up(x_train_up)
            z_down = np.polyfit(np.array(x_train_down).squeeze(),np.log(495-np.array(y_train_down).squeeze()),1)
            p_down = np.poly1d(z_down)
            logy_p_down = p_down(x_train_down)
            if printout == True:
                print('Exponential model for upper part: 495 - e^('+str(z_up[0])+' *P + '+str(z_up[1])+')')
                print('Exponential model for lower part: 495 - e^('+str(z_down[0])+' *P + '+str(z_down[1])+')')
                mean_up = np.sum(np.array(y_train_up))/len(y_train_up)
                mean_down = np.sum(np.array(y_train_down))/len(y_train_down)
                sum_up = np.sum((np.array(y_train_up).transpose()-(495-np.exp(logy_p_up.squeeze())))**2)
                sum_down = np.sum((np.array(y_train_down).transpose()-(495-np.exp(logy_p_down.squeeze())))**2)
                divide_up = np.sum((np.array(y_train_up)-mean_up)**2)
                divide_down = np.sum((np.array(y_train_down)-mean_down)**2)
                std_up = np.array(y_train_up).squeeze()-(495-np.exp(logy_p_up.squeeze()))
                std_down = np.array(y_train_down).squeeze()-(495-np.exp(logy_p_down.squeeze()))
                print('R2(Total):', 1-(sum_up+sum_down)/(divide_up+divide_down))
                print('Standard error(Total):', np.std(np.append(std_up,std_down)))
                plt.scatter(np.array(x_train_up), 495-np.exp(logy_p_up.squeeze()),c='r',label='Exponential_upper')
                plt.scatter(np.array(x_train_down), 495-np.exp(logy_p_down.squeeze()),c='orange',label='Exponential_lower')
                plt.scatter(np.array(x_train_up),np.array(y_train_up),c='b',alpha=0.5,label='Measurement_upper')
                plt.scatter(np.array(x_train_down),np.array(y_train_down),c='g',alpha=0.5,label='Measurement_lower')
                plt.legend()
                plt.xlabel('Pressure (Psi)')
                plt.ylabel('Flex (v)')
                plt.show()
            return z_up,z_down
        
    def auto_PID(self,separate=False,disp=False,case='step'):
        """Solve and simulate the PID control system with automatically determined PID parameters
        
        Parameters
        ----------
            separate: bool
                Choose to separate the dataset into two parts and derive two exponential regression models or not, this will also deactivate other regression models 
            disp: bool
                Choose to print out the process of solving the PID control system or not
        
        Raises
        ----------
            TypeError: If separate or printout is not a boolean value, or if case is not a string
            ValueError: If case is not 'step' or 'sine'
            
        """
        if not isinstance(separate, bool):
            raise TypeError('separate must be True or False')
        if not isinstance(disp, bool):
            raise TypeError('disp must be True or False')
        if not isinstance(case, str):
            raise TypeError('case must be a string')
        if case != 'step' and case != 'sine':
            raise ValueError('case must be "step" or "sine"')
        # Get model of estimated angle and model of flex
        lin_reg = self.get_theta_linearmodel(inputs=3,standardization=False,subtraction=False,printout=False)
        if separate == False:
            z = self.get_flex_models(separate,printout=False)
        else:
            z_up,z_down = self.get_flex_models(separate,printout=False)
            
        m = GEKKO()
        tf = 40
        m.time = np.linspace(0,tf,2*tf+1)
        step = np.zeros(2*tf+1)
        if case == 'step':
            step[0:16] = 30
            step[16:32] = 35
            step[32:48] = 40
            step[48:64] = 35
            step[64:] = 30
        else:
            step[20:]  = [30 + 10*np.sin(2*np.pi*(i)/len(step[20:])) for i in range(len(step[20:]))]

        # Controller model
        C_p = lin_reg.coef_.squeeze()[0]                    
        C_f = lin_reg.coef_.squeeze()[1]
        C_phi = lin_reg.coef_.squeeze()[2]
        phi = m.Param(value=0)  # Tilting angle, 0 means no orientation
        Kc = m.FV(value=5.0, lb=1.0, ub=1000.0)  # Controller gain
        Kc.STATUS = 1
        tauI = m.FV(value=2.0, lb=0.1, ub=100.0)  # Integral constant
        tauI.STATUS = 1
        tauD = m.FV(value=1.0, lb=0.1, ub=100.0)  # Derivative constant
        tauD.STATUS = 1
        P_0 = m.Const(value=0.0)   # Bias of pressure
        P = m.Var(value=0.0)       # Pressure
        if separate == False:
            F = m.Var(value=495 - np.exp(z[0]*P.value + z[1]))  # Flex
        else:
            F = m.Var(value=495 - (np.exp(z_down[0]*P.value + z_down[1])+np.exp(z_up[0]*P.value + z_up[1]))/2)  # Flex
            F_equ = m.if3(P.dt(),495 - m.exp(z_down[0]*P + z_down[1]),495 - m.exp(z_up[0]*P + z_up[1]))
        PV = m.Var(value=C_p*0 + C_f*F.value + C_phi*phi.value + lin_reg.intercept_[0])  # Process variable, represent estimated angle
        SP = m.Param(value=step)  # Set point, represent target angle
        Intgrl = m.Var(value=0.0)  # Integral of the error
        err = m.Intermediate(SP-PV)  # Set point error
        m.Equation(Intgrl.dt()==err)  # Integral of the error
        m.Equation(P == P_0 + Kc*err + (Kc/tauI)*Intgrl - Kc*tauD*PV.dt())  #  Equation of PID controller
        m.Obj(err**2)  # The goal is to minimize the square of set point error

        # Process model
        if separate == False:
            m.Equation(F == 495 - m.exp(z[0]*P + z[1]))
        else:
            m.Equation(F == F_equ)
        m.Equation(PV == C_p*P + C_f*F + C_phi*phi + lin_reg.intercept_[0])
        m.options.IMODE=6  # Dynamic Simultaneous Control mode
        m.options.MAX_ITER=1e3 # Maximum number of iterations
        m.solve(disp)

        print('The PID system can be automatically solved')
        print('Kc:  ' + str(Kc.value[0]))
        print('tauI:  ' + str(tauI.value[0]))
        print('tauD:  ' + str(tauD.value[0]))
        plt.figure(figsize=(5,5))
        plt.subplot(2,1,1)
        plt.plot(m.time,P.value,'b:',label='PID output (Pressure)')
        plt.ylabel('Pressure (Psi)')
        plt.legend()
        plt.subplot(2,1,2)
        plt.plot(m.time,SP.value,'k-',label='Target angle')
        plt.plot(m.time,PV.value,'r--',label='Output angle')
        plt.xlabel('Time (sec)')
        plt.ylabel('Angle (deg)')
        plt.legend()
        plt.show()
        
    def manual_PID(self,separate=False,disp=False,case='step',Kc=26.0,tauI=(26.0/11.0),tauD=(0.2/26.0)):
        """Solve and simulate the PID control system with manually determined PID parameters
        
        Parameters
        ----------
            separate: bool
                Choose to separate the dataset into two parts and derive two exponential regression models or not, this will also deactivate other regression models 
            disp: bool
                Choose to print out the process of solving the PID control system or not
            Kc: float
                Controller gain
            tauI: float
                Integral constant
            tauD: float
                Derivative constant
        
        Raises
        ----------
            TypeError: If separate or printout is not a boolean value, or if case is not a string, or if any of the PID paramters Kc, tauI or tauD is not a float
            ValueError: If case is not 'step' or 'sine', or if any of the PID paramters Kc,tauI,tauD is smaller than 0
        """
        if not isinstance(separate, bool):
            raise TypeError('separate must be True or False')
        if not isinstance(disp, bool):
            raise TypeError('disp must be True or False')
        if not isinstance(case, str):
            raise TypeError('case must be a string')
        if not isinstance(Kc, float):
            raise TypeError('Kc must be a float')
        if not isinstance(tauI, float):
            raise TypeError('tauI must be a float')
        if not isinstance(tauD, float):
            raise TypeError('tauD must be a float')
        if case != 'step' and case != 'sine':
            raise ValueError('case must be "step" or "sine"')
        if Kc<0 or tauI<0 or tauD<0:
            raise ValueError('PID parameters are generally positive values')
        # Get model of estimated angle and model of flex
        lin_reg = self.get_theta_linearmodel(inputs=3,standardization=False,subtraction=False,printout=False)
        if separate == False:
            z = self.get_flex_models(separate,printout=False)
        else:
            z_up,z_down = self.get_flex_models(separate,printout=False)
            
        m = GEKKO()
        tf = 40
        m.time = np.linspace(0,tf,2*tf+1)
        step = np.zeros(2*tf+1)
        if case == 'step':
            step[0:16] = 30
            step[16:32] = 35
            step[32:48] = 40
            step[48:64] = 35
            step[64:] = 30
        else:
            step[20:]  = [30 + 10*np.sin(2*np.pi*(i)/len(step[20:])) for i in range(len(step[20:]))]

        # Controller model
        C_p = lin_reg.coef_.squeeze()[0]                    
        C_f = lin_reg.coef_.squeeze()[1]
        C_phi = lin_reg.coef_.squeeze()[2]
        phi = m.Param(value=0)  # Tilting angle, 0 means no orientation
        P_0 = m.Const(value=0.0)   # Bias of pressure
        P = m.Var(value=0.0)       # Pressure
        if separate == False:
            F = m.Var(value=495 - np.exp(z[0]*P.value + z[1]))  # Flex
        else:
            F = m.Var(value=495 - (np.exp(z_down[0]*P.value + z_down[1])+np.exp(z_up[0]*P.value + z_up[1]))/2)  # Flex
            F_equ = m.if3(P.dt(),495 - m.exp(z_down[0]*P + z_down[1]),495 - m.exp(z_up[0]*P + z_up[1]))
        PV = m.Var(value=C_p*0 + C_f*F.value + C_phi*phi.value + lin_reg.intercept_[0])  # Process variable, represent estimated angle
        SP = m.Param(value=step)  # Set point, represent target angle
        Intgrl = m.Var(value=0.0)  # Integral of the error
        err = m.Intermediate(SP-PV)  # Set point error
        m.Equation(Intgrl.dt()==err)  # Integral of the error
        m.Equation(P == P_0 + Kc*err + (Kc/tauI)*Intgrl - Kc*tauD*PV.dt())  #  Equation of PID controller
        
        # Process model
        if separate == False:
            m.Equation(F == 495 - m.exp(z[0]*P + z[1]))
        else:
            m.Equation(F == F_equ)
        m.Equation(PV == C_p*P + C_f*F + C_phi*phi + lin_reg.intercept_[0])
        m.options.IMODE=5  # Dynamic Simultaneous Estimation mode
        m.solve(disp)

        print('The PID system can be manually solved')
        plt.figure(figsize=(5,5))
        plt.subplot(2,1,1)
        plt.plot(m.time,P.value,'b:',label='PID output (Pressure)')
        plt.ylabel('Pressure (Psi)')
        plt.legend()
        plt.subplot(2,1,2)
        plt.plot(m.time,SP.value,'k-',label='Target angle')
        plt.plot(m.time,PV.value,'r--',label='Output angle')
        plt.xlabel('Time (sec)')
        plt.ylabel('Angle (deg)')
        plt.legend()
        plt.show()